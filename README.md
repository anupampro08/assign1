# Assignment 1
## Q1. Convert Hexadecimal to binary (24F)

Converting 24F to binary as,

2 4 F	=	0010 0100 1111

Therefore, (24F)16 = (001001001111)2

## Q2. Convert any one Octal to Binary

(403)8

4 0 3	=	100 000 011

Therefore, (356)8	=	(100000011)2


## Q3. Create Inputs and outputs Domino’s Pizza Store and website.

Inputs – 
\
a.	Customer data ( Name, phone number, email address)\
b.	Store Location\
c.	Orders\
d.	Customer Address\
e.	Payment Methods\
f.	Delivery type\
\
\
Outputs – 
\
a.	Customer Account\
b.	Order Receipt\
c.	Live Location of the Delivery Agent\
d.	Tracking Order details\
e.	Cashback of coupons for next order\
f.	 Email or SMS with all the order details



## Q4. Create a new Repository –
-add 2 files file1.txt and file2.txt\
-commit 2 files\
-modify file2.txt and commit\

Steps: 
\
1 .Create a new repository.\
2 . Create two file and add this two files to the staging area\
3 . Committed both the files.\
4 . Modify the file2.txt with the help of command line or any text editor\
5 . Commit the changes


## Q5. Create logic function to compare 2 sets of numbers and show which is greater. 

Input Variables: var1, var2

Logic Function:
\
isGreater(var1, var2):\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if var1>var2\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;display var1\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;else if var2>var1\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;display var2\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;else\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;display “both are equal”
